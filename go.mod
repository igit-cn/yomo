module github.com/yomorun/yomo

go 1.15

require (
	github.com/cenkalti/backoff/v4 v4.1.1
	github.com/lucas-clemente/quic-go v0.21.0
	github.com/reactivex/rxgo/v2 v2.5.0
	github.com/spf13/cobra v1.1.3
	github.com/yomorun/y3-codec-golang v1.6.9
	golang.org/x/tools v0.0.0-20191125144606-a911d9008d1f // indirect
	gopkg.in/yaml.v2 v2.4.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
